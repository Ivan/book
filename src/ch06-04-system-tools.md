# System Tools

## Coreutils

Coreutils is a collection of basic command line utilities included with Redox (or with Linux, BSD, etc.). This includes programs like `ls`, `cp`, `cat` and various other tools necessary for basic command line interaction.

Redox's coreutils aim to be more minimal than, for instance, the GNU coreutils included with most Linux systems.

## Binutils

Binutils contains utilities for manipulating binary files. Currently Redox's binutils includes `strings`, `disasm`, `hex`, and `hexdump`

## Extrautils

Some additional command line tools are included in extrautils, such as `less`, `grep`, and `dmesg`.

## Contain

This program provides containers (namespaces) on Redox.

- [Repository](https://gitlab.redox-os.org/redox-os/contain)

## acid

The stress test suite of Redox to detect crashes/bugs.

- [Repository](https://gitlab.redox-os.org/redox-os/acid)

## resist

The POSIX test suite of Redox to see how much % the system is compliant to the [POSIX](https://en.wikipedia.org/wiki/POSIX) specification (more means better software portability).

- [Repository](https://gitlab.redox-os.org/redox-os/resist)
