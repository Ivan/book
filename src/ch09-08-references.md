# References

This page will list references for OS development and Rust programming to help developers.

### Rust

- [Rust Book](https://doc.rust-lang.org/stable/book/) - The most important source of information on the Rust programming language.
- [Rust By Example](https://doc.rust-lang.org/stable/rust-by-example/) - Learn Rust with examples.
- [Rustlings](https://github.com/rust-lang/rustlings) - Learn Rust with exercises.
- [Awesome Rust](https://github.com/rust-unofficial/awesome-rust) - Curated list of Rust programs, libraries and resources.
- [No Boilerplate - Rust playlist](https://www.youtube.com/playlist?list=PLZaoyhMXgBzoM9bfb5pyUOT3zjnaDdSEP) - Amazing short documentaries about special things on Rust.
- [Developer Roadmaps - Rust](https://roadmap.sh/rust) - A guide to learn Rust.
- [rust-learning](https://github.com/ctjhoa/rust-learning) - A list with articles and videos to learn Rust.
- [Rust Playground](https://play.rust-lang.org/) - Test your Rust code on the web browser.
- [This Week in Rust](https://this-week-in-rust.org/) - Weekly updates on the Rust ecosystem, it covers language improvements, organization updates, community updates and articles.
- [The Coded Message - Rust articles](https://www.thecodedmessage.com/tags/rust/) - Excellent Rust articles.
- [fasterthanlime - Rust articles](https://fasterthanli.me/tags/rust)

### OS development

- [OSDev wiki](https://wiki.osdev.org/Expanded_Main_Page) - The best wiki about OS development of the world.
- [Writing an OS in Rust](https://os.phil-opp.com/) - Blog series to write an operating system in Rust.
- [Rust OSDev](https://rust-osdev.com/) - Monthly reports with updates on the Rust low-level ecosystem libraries and operating systems.

### Manual Pages

- [FreeBSD Manual Pages](https://man.freebsd.org/cgi/man.cgi) - Powerful source for Unix/BSD documentation.
- [Linux Manual Pages (man7)](https://www.man7.org/linux/man-pages/) - Very popular source for Linux documentation.
- [Linux Manual Pages (die.net)](https://linux.die.net/man/) - Another popular source for Linux documentation.

### Computer Science

- [GeeksforGeeks](https://www.geeksforgeeks.org/) - A computer science portal with many articles for several areas and tasks.
- [computer-science](https://github.com/ossu/computer-science) - A list for computer science education.
- [Developer Roadmaps - Computer Science](https://roadmap.sh/computer-science) - A guide to learn computer science.
- [Minix](https://minix3.org/) - On the Minix website you can find great papers and articles about the microkernel architecture and reliable systems.
- [Plan 9](https://plan9.io/plan9/) - On the Plan 9 website you can find papers and documentation about distributed systems.
- [seL4](https://sel4.systems/) - On the seL4 website you can find papers and documentation about a secure and fast microkernel design.
- [The Coded Message - Computing/Programming articles](https://www.thecodedmessage.com/tags/computers)