# Chat

The best way to communicate with the Redox team is on Matrix Chat. You can open the [Redox Space](https://matrix.to/#/#redox:matrix.org) and see the  rooms that are available (these rooms are English-only, we don't accept other languages because we don't understand them).

Matrix has several different clients. [Element](https://element.io/) is a commonly used choice.

We follow the [Rust Code Of Conduct](https://www.rust-lang.org/policies/code-of-conduct) as rules of the chat rooms.

(You must join the "Join Requests" room and request an invite to the Redox space, only space members can see all rooms)

All rooms available on the Redox space:

- [#redox-join:matrix.org](https://matrix.to/#/#redox-join:matrix.org) - a room to be invited to Redox space.
- [#redox-general:matrix.org](https://matrix.to/#/#redox-general:matrix.org) - a room for Redox-related discussions (questions, suggestions, porting, etc).
- [#redox-dev:matrix.org](https://matrix.to/#/#redox-dev:matrix.org) - a room for the development, here you can talk about anything development-related (code, proposals, achievements, styling, bugs, etc).
- [#redox-support:matrix.org](https://matrix.to/#/#redox-support:matrix.org) - a room for testing/building support (problems, errors, questions).
- [#redox-mrs:matrix.org](https://matrix.to/#/#redox-mrs:matrix.org) - a room to send all ready merge requests without conflicts  (if you have a ready MR to merge, send there).
- [#redox-gitlab:matrix.org](https://matrix.to/#/#redox-gitlab:matrix.org) - a room to send new GitLab accounts for approval.
- [#redox-board:matrix.org](https://matrix.to/#/#redox-board:matrix.org) - a room for the Board of Directors meetings.
- [#redox-voip:matrix.org](https://matrix.to/#/#redox-voip:matrix.org)
- [#redox-random:matrix.org](https://matrix.to/#/#redox-random:matrix.org) - a room for off-topic.

(We recommend that you leave the "Join Requests" room after your entry on Redox space)
